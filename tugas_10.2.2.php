<!DOCTYPE html>
<html>
    <head>
        <title>nestedloop</title>
    </head>
    <body>
        <pre>
            <?php
                $baris = 0;
                $i = 0;
                $j = 0;
                $nilai = isset($_POST['tinggi']) ? $_POST['tinggi'] : 5;
                $tinggi = intval($nilai);
                for ($baris = 1; $baris <= $tinggi; $baris++) {
                    for ($i = 1; $i <= $tinggi - $baris; $i++) {
                        echo " ";
                    }
                    for ($j = 1; $j < 2 * $baris; $j++) {
                        echo "*";
                    }
                    echo "\n";
                }
            ?>
        </pre>
        <form method="POST" action="">
            <label for="tinggi">Masukkan tinggi:</label>
            <input type="text" name="tinggi" id="tinggi" required>
            <button type="submit">Submit</button>
        </form>
    </body>
</html>
