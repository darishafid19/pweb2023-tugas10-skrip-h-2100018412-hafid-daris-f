<!DOCTYPE html>
<html>
    <head>
        <title>Konversi Nilai</title>
    </head>
    <body>
        <h1>Konversi Nilai Angka ke Nilai Huruf</h1>
        <form method="POST" action="">
            <label for="nilai">Masukkan Nilai Angka:</label>
            <input type="text" name="nilai" id="nilai" required>
            <button type="submit">Konversi</button>
        </form>

        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $nilai = $_POST["nilai"];
                $nilai_huruf = "";

                if ($nilai >= 4.00) {
                    $nilai_huruf = "A";
                } elseif ($nilai >= 3.67) {
                    $nilai_huruf = "A-";
                } elseif ($nilai >= 3.33) {
                    $nilai_huruf = "B+";
                } elseif ($nilai >= 3.00) {
                    $nilai_huruf = "B";
                } elseif ($nilai >= 2.67) {
                    $nilai_huruf = "B-";
                } elseif ($nilai >= 2.33) {
                    $nilai_huruf = "C+";
                } elseif ($nilai >= 2.00) {
                    $nilai_huruf = "C";
                } elseif ($nilai >= 1.67) {
                    $nilai_huruf = "C-";
                } elseif ($nilai >= 1.33) {
                    $nilai_huruf = "D+";
                } elseif ($nilai >= 1.00) {
                    $nilai_huruf = "D";
                } else {
                    $nilai_huruf = "E";
                }

                echo "<h2>Hasil Konversi</h2>";
                echo "Nilai angka anda: $nilai <br>";
                echo "Nilai huruf anda: $nilai_huruf <br>";
            }
        ?>
    </body>
</html>
